import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import CategoryDto from 'src/dtos/category.dto';
import CreateCategoryDto from 'src/dtos/createCategory.dto';
import { CategoryService } from './category.service';

@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) { }

  @Get()
  async getCategories(): Promise<Array<CategoryDto>> {
    let response: Array<CategoryDto> = await this.categoryService.getCategories();
    return response;
  }

  @Get(':categoryId')
  async getCategory(@Param('categoryId') categoryId: string): Promise<CategoryDto> {
    let response: CategoryDto = await this.categoryService.getCategory(categoryId);
    return response;
  }

  @Post()
  async createCategory(@Body() createCategory: CreateCategoryDto): Promise<CategoryDto> {
    let response: CategoryDto = await this.categoryService.createCategory(createCategory);
    return response;
  }
}
