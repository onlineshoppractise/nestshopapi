import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Model } from 'mongoose';
import CategoryDto from 'src/dtos/category.dto';
import CreateCategoryDto from 'src/dtos/createCategory.dto';

@Injectable()
export class CategoryService {
  constructor(@InjectModel('Category') private readonly categoryModel: Model<CategoryDto>) { }

  public async createCategory(createCategory: CreateCategoryDto): Promise<CategoryDto> {
    let categoryModel: CategoryDto = await this.categoryModel.create({ name: createCategory.name });

    return { _id: categoryModel._id, name: categoryModel.name };
  }

  public async getCategory(categoryId: string): Promise<CategoryDto> {
    if (!mongoose.Types.ObjectId.isValid(categoryId)) {
      return null;
    }
    let category: CategoryDto = await this.categoryModel.findOne({ _id: new mongoose.Types.ObjectId(categoryId) });

    if (category) {
      return { _id: category._id, name: category.name };
    }

    return null;
  }

  public async getCategories(): Promise<Array<CategoryDto>> {
    let categories: Array<CategoryDto> = await this.categoryModel.find();

    return categories.map(x => { return { _id: x._id, name: x.name } });
  }
}
