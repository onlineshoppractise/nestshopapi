import CharacteristicDto from "./characteristic.dto"

class CreateProductDto {
  title: string
  description: string
  characteristics: Array<CharacteristicDto>
  categoryId: string
} export default CreateProductDto;