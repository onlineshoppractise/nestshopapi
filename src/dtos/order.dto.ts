import OrderItemDto from "./orderItem.dto";

class OrderDto {
  order_id: number
  items: Array<OrderItemDto>
} export default OrderDto;