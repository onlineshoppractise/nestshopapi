class OrderItemDto {
  productId: string
  count: number
} export default OrderItemDto;