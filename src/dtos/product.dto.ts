import mongoose from "mongoose"
import CategoryDto from "./category.dto"
import CharacteristicDto from "./characteristic.dto"

class ProductDto {
  _id: mongoose.Types.ObjectId
  title: string
  description: string
  characteristics: Array<CharacteristicDto>
  category: CategoryDto
} export default ProductDto;