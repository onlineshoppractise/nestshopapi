import mongoose from "mongoose";

class CategoryModel {
  _id: mongoose.Types.ObjectId
  name: string
} export default CategoryModel;