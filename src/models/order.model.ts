import mongoose from "mongoose";
import OrderItemModel from "./orderItem.model";
import ShippingDetailsModel from "./shippingDetails.model";

class OrderModel {
  _id: mongoose.Types.ObjectId
  orderId: number
  items: Array<OrderItemModel>
  shippingDetails: ShippingDetailsModel
} export default OrderModel;