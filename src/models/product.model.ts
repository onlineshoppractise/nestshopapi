import mongoose from "mongoose";
import CategoryModel from "./category.model";
import CharacteristicModel from "./characteristic.model";

class ProductModel {
  _id: mongoose.Types.ObjectId
  title: string
  description: string
  characteristics: Array<CharacteristicModel>
  category: CategoryModel
} export default ProductModel;