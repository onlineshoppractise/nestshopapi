class ShippingDetailsModel {
  firstName: string;
  secondName: string;
  phoneNumber: string;
  email: string;
  shippingAddress: string;
} export default ShippingDetailsModel;