import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { query } from 'express';
import CreateProductDto from 'src/dtos/createProduct.dto';
import ProductDto from 'src/dtos/product.dto';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) { }

  @Get()
  async getProducts(@Query() query): Promise<Array<ProductDto>> {
    let categoryId = query['categoryId'];
    let offset = query['offset'] ? query['offset'] : 0;
    let limit = query['limit'] ? query['limit'] : 10;
    return await this.productService.getProductsList(categoryId, offset, limit);
  }

  @Get(':productId')
  async getProduct(@Param('productId') productId: string): Promise<ProductDto> {
    return await this.productService.getProduct(productId);
  }

  @Post()
  async createProducts(@Body() createProduct: CreateProductDto): Promise<ProductDto> {
    return await this.productService.createProduct(createProduct);
  }
}
