import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CategorySchema } from 'src/schemas/category.schema';
import { ProductSchema } from 'src/schemas/product.schema';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Product", schema: ProductSchema }]),
    MongooseModule.forFeature([{ name: "Category", schema: CategorySchema }])
  ],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule { }
