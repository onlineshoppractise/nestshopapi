import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Model } from 'mongoose';
import CharacteristicDto from 'src/dtos/characteristic.dto';
import CreateProductDto from 'src/dtos/createProduct.dto';
import ProductDto from 'src/dtos/product.dto';
import CategoryModel from 'src/models/category.model';
import ProductModel from 'src/models/product.model';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel('Product') private readonly productModel: Model<ProductModel>,
    @InjectModel('Category') private readonly categoryModel: Model<CategoryModel>) { }

  public async getProduct(productId: string): Promise<ProductDto> {
    let product = await this.productModel.findOne({ _id: productId });
    let category = await this.categoryModel.findOne({ _id: product.category });
    product.category = category;
    return this.getDtoFromModel(product);
  }

  public async getProductsList(categoryId: string, offset: number, limit: number): Promise<Array<ProductDto>> {
    let category = await this.categoryModel.findOne({ _id: categoryId });
    let productModels: Array<ProductModel>;
    if (category) {
      productModels = await this.productModel.find({ category: category }).skip(offset).limit(limit);
    } else {
      productModels = await this.productModel.find().skip(offset).limit(limit);
    }

    let products: Array<ProductDto> = productModels.map(x => this.getDtoFromModel(x));

    return products;
  }

  public async createProduct(createProduct: CreateProductDto): Promise<ProductDto> {
    let category = await this.categoryModel.findOne({ _id: createProduct.categoryId });
    let prductModel: ProductModel = {
      _id: new mongoose.Types.ObjectId(),
      title: createProduct.title,
      description: createProduct.description,
      characteristics: createProduct.characteristics,
      category: category
    };
    let product = await this.productModel.create(prductModel);

    return this.getDtoFromModel(product);
  }

  getDtoFromModel(product: ProductModel): ProductDto {
    return {
      _id: product._id,
      title: product.title,
      description: product.description,
      characteristics: product.characteristics.map<CharacteristicDto>(x => {
        return { name: x.name, value: x.value }
      }),
      category: product.category ? {
        _id: product.category._id.toJSON(),
        name: product.category.name
      } : null
    }
  }
}
