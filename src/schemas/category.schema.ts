import mongoose from "mongoose";
import CategoryModel from "src/models/category.model";

export const CategorySchema = new mongoose.Schema<CategoryModel>({
  name: String
});