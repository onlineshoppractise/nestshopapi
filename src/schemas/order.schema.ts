import mongoose from "mongoose";
import OrderModel from "src/models/order.model";

export const OrderSchema = new mongoose.Schema<OrderModel>({
  orderId: Number,
  items: [{ type: mongoose.Types.ObjectId, ref: 'Product' }],
  shippingDetails: {
    firstName: String,
    secondName: String,
    phoneNumber: String,
    email: String,
    shippingAddress: String
  }
});