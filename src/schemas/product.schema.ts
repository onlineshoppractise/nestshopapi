import mongoose from "mongoose";
import ProductModel from "src/models/product.model";

export const ProductSchema = new mongoose.Schema<ProductModel>({
  title: String,
  description: String,
  characteristics: [
    { name: String, value: String }
  ],
  category: { type: mongoose.Types.ObjectId, ref: 'Category' }
});